# ajilemondrop's for of suckless utils

## How to use

    git clone https://codeberg.org/ajilemondrop/suckless-utils
    cd suckless-utils
    cd <desired util>
    sudo make clean install

## Modification

basically all surface level modifications are done by editing config.h and recompiling
if you want to modify the functioning of the util, edit <name>.c (so for dwm it would be dwm.c)

## Why

You can use this repo for inspiration, for me it is basically useless now.

### Have fun!
