/*       _ _ _                                _                 
        (_|_) |                              | |                
       _ _ _| | ___ _ __ ___   ___  _ __   __| |_ __ ___  _ __  
   / _` | | | |/ _ \ '_ ` _ \ / _ \| '_ \ / _` | '__/ _ \| '_ \ 
  | (_| | | | |  __/ | | | | | (_) | | | | (_| | | | (_) | |_) |
   \__,_| |_|_|\___|_| |_| |_|\___/|_| |_|\__,_|_|  \___/| .__/ 
       _/ |                                              | |    
      |__/                                               |_|    
*/

/* See LICENSE file for copyright and license details. */
/* Default settings; can be overriden by command line. */

static int topbar = 0;                      /* -b  option; if 0, dmenu appears at bottom     */
/* -fn option overrides fonts[0]; default X11 font or font set */
static const char *fonts[] = {
	"mononoki nerd font:size=11:style=Bold"	
};
static const char *prompt      = NULL;      /* -p  option; prompt to the left of input field */
static const char col_bg[]	= "#1f1e29";
static const char col_purple[]	= "#bd93f9";
static const char col_blue[]	= "#8be9fd";
static const char col_text[]	= "#f8f8f2";
static const char *colors[SchemeLast][2] = {
	/*     fg         bg       */
	[SchemeNorm] = { col_text, col_bg },
	[SchemeSel] = { col_bg, col_purple },
	[SchemeOut] = { col_bg, col_blue },
};
/* -l option; if nonzero, dmenu uses vertical list with given number of lines */
static unsigned int lines      = 0;
/* -h option; minimum height of a menu line */
static unsigned int lineheight = 24;
static unsigned int min_lineheight = 12;

/*
 * Characters not considered part of a word while deleting words
 * for example: " /?\"&[]"
 */
static const char worddelimiters[] = " ";

/* Size of the window border */
static unsigned int border_width = 0;
