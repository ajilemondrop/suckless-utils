dwm - dynamic window manager
============================
dwm is an extremely fast, small, and dynamic window manager for X.


Requirements
------------
In order to build dwm you need the Xlib header files.


Installation
------------
Edit config.mk to match your local setup (dwm is installed into
the /usr/local namespace by default).

Afterwards enter the following command to build and install dwm (if
necessary as root):

    make clean install


Running dwm
-----------
Add the following line to your .xinitrc to start dwm using startx:

    exec dwm

In order to connect dwm to a specific display, make sure that
the DISPLAY environment variable is set correctly, e.g.:

    DISPLAY=foo.bar:1 exec dwm

(This will start dwm on display :1 of the host foo.bar.)

dwm will automatically run an autostart script located at ~/.config/autostart/autostart.sh
add necessary items as needed (this version does not come with default app keybindings
except dmenu).

In the simplest form, it should look something like this:

    #!/bin/bash
    exec sxhkd &

after finishing, make it executable by running:

    chmod +x ~/.config/autostart/autostart.sh

Recommended Software
--------------------
st (simple terminal), dmenu (is used by default for app menu),
dwmblocks (shows status bar info), sxhkd (allows adding keybindings)

Help
----
If you feel lost, open a terminal and type

    man dwm

There you should find most of the basics.

Configuration
-------------
The configuration of dwm is done by creating a custom config.h
and (re)compiling the source code.
