/*        _ _ _                                _                 
*        (_|_) |                              | |                
*       _ _ _| | ___ _ __ ___   ___  _ __   __| |_ __ ___  _ __  
*   / _` | | | |/ _ \ '_ ` _ \ / _ \| '_ \ / _` | '__/ _ \| '_ \ 
*  | (_| | | | |  __/ | | | | | (_) | | | | (_| | | | (_) | |_) |
*   \__,_| |_|_|\___|_| |_| |_|\___/|_| |_|\__,_|_|  \___/| .__/ 
*       _/ |                                              | |    
*      |__/                                               |_|    
*/

/* See LICENSE file for copyright and license details. */

// #include <X11/XF86keysym.h>

/* alt-tab configuration */
static const unsigned int tabModKey 			= 0x40;	/* if this key is hold the alt-tab functionality stays acitve. This key must be the same as key that is used to active functin altTabStart `*/
static const unsigned int tabCycleKey 			= 0x17;	/* if this key is hit the alt-tab program moves one position forward in clients stack. This key must be the same as key that is used to active functin altTabStart */
static const unsigned int tabPosY 			= 1;	/* tab position on Y axis, 0 = bottom, 1 = center, 2 = top */
static const unsigned int tabPosX 			= 1;	/* tab position on X axis, 0 = left, 1 = center, 2 = right */
static const unsigned int maxWTab 			= 800;	/* tab menu width */
static const unsigned int maxHTab 			= 400;	/* tab menu height */

#define SESSION_FILE "/tmp/dwm-session"

/* appearance */
static const unsigned int borderpx  = 3;        /* border pixel of windows */
static const unsigned int gappx     = 8;        /* gap pixel between windows */
static const unsigned int snap      = 4;        /* snap pixel */
static const unsigned int systraypinning = 0;   /* 0: sloppy systray follows selected monitor, >0: pin systray to monitor X */
static const unsigned int systrayonleft  = 0;  	/* 0: systray in the right corner, >0: systray on left of status text */
static const unsigned int systrayspacing = 2;   /* systray spacing */
static const int systraypinningfailfirst = 1;   /* 1: if pinning fails, display systray on the first monitor, False: display systray on the last monitor*/
static const int showsystray        = 1;        /* 0 means no systray */
static const int showbar            = 1;        /* 0 means no bar */
static const int topbar             = 0;        /* 0 means bottom bar */
static const int focusonwheel       = 0;	/* O means focus on click*/
static const int user_bh            = 24;       /* 0 means that dwm will calculate bar height, >= 1 means dwm will user_bh as bar height */
static const char *fonts[]          = {"mononoki nerd font:size=11:style=Bold"};
static const char dmenufont[]       = "mononoki nerd font:size=11:style=Bold";
static const char col_purple[]	    = "#bd93f9";
static const char col_bg[]	    = "#1e1f29";
static const char col_text[]	    = "#f8f8f2";
static const char col_dark[]	    = "#6272a4";
static const char *colors[][3]      = {
	/*               fg         bg               border   */
	[SchemeNorm] = { col_text,  col_bg,  col_bg},
	[SchemeSel]  = { col_purple,  col_bg,  col_purple},
	[SchemeHid]  = { col_dark, col_bg, col_purple},
	};

static const char *const autostart[] = {
	".config/autostart/autostart.sh", NULL,
	NULL									/* terminate */
};

/* tagging */
//static const char *tags[] = { "", "", "", "", "", "", "", "", ""};
//static const char *tags[] = { "", "", "", "", "", "", "", "", "" };
static const char *tags[] = { "1", "2", "3", "4", "5", "6", "7", "8", "9"};

static const unsigned int ulinepad	= 5;	/* horizontal padding between the underline and tag */
static const unsigned int ulinestroke	= 5;	/* thickness / height of the underline */
static const unsigned int ulinevoffset	= 0;	/* how far above the bottom of the bar the line should appear */
static const int ulineall 		= 0;	/* 1 to show underline on all tags, 0 for just the active ones */
static const unsigned int underlinestroke = 2;  /* thickness of the underline for focuesed and hidden windows */
static const unsigned int lupadding	= 5;	/* left underline padding between items */
static const int rectanglearound	= 0;	/* 0 to draw a bar under window names, else a rectangle is drawn around */

static const char *tagsel[][2] = { 		/* colours of tags, {foreground, background} */
	{ col_text, col_bg },
	{ col_text, col_bg },
	{ col_text, col_bg },
	{ col_text, col_bg },
	{ col_text, col_bg },
	{ col_text, col_bg },
	{ col_text, col_bg },
	{ col_text, col_bg },
	{ col_text, col_bg }

};

static const Rule rules[] = {
	/* xprop(1):
	 *	WM_CLASS(STRING) = instance, class
	 *	WM_NAME(STRING) = title
	 */
	/* class      instance    title       tags mask     isfloating   monitor */
	{ "Nm-connection-editor", NULL, NULL, 0,	    1,		 -1 },
	{ "Blueman-manager", 	  NULL,	NULL, 0,	    1,		 -1 },
	{ "Galculator"		, NULL, NULL, 0,	    1,		 -1 },
	{ "Pavucontrol",	  NULL, NULL, 0,	    1,		 -1 },
	{ "st-256color", NULL, "pulsemixer",  0,	    1,		 -1 },
};

/* layout(s) */
static const float mfact     = 0.50; /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;    /* number of clients in master area */
static const int resizehints = 1;    /* 1 means respect size hints in tiled resizals */
static const int decorhints  = 0;    /* 1 means respect decoration hints */
static const int attachbelow = 1;    /* 1 means attach after the currently active window */
static const int lockfullscreen = 1; /* 1 will force focus on the fullscreen window */

static void focusurgent (const Arg *arg);

static const Layout layouts[] = {
	/* symbol     arrange function */
	{ "[]=",      tile },    /* first entry is default */
	{ "><>",      NULL },    /* no layout function means floating behavior */
	{ "[M]",      monocle },
	{ "[D]",      deck },
};

/* key definitions */
#define MODKEY Mod1Mask
#define TAGKEYS(KEY,TAG) \
	{ MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} }, \
        { Mod4Mask,                     KEY,      focusnthmon,    {.i  = TAG } }, \
        { Mod4Mask|ShiftMask,           KEY,      tagnthmon,      {.i  = TAG } },


/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* commands */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
static const char *dmenucmd[] = { "dmenu_run", "-m", dmenumon, "-fn", dmenufont, "-nb", col_bg, "-nf", col_text, "-sb", col_purple, "-sf", col_bg, NULL };
static const char *powermenu [] = { "logout-script.sh", NULL };

static Key keys[] = {
	/* modifier                     key        function        argument */
	{ MODKEY,                       33,	   spawn,          {.v = dmenucmd } },		// p
	{ Mod1Mask|ControlMask,		119,	   spawn,	   {.v = powermenu} },		// Delete
	{ MODKEY,                       56,        togglebar,      {0} },			// b
	{ MODKEY|ShiftMask,             44,        rotatestack,    {.i = +1 } },		// j
	{ MODKEY|ShiftMask,             45,        rotatestack,    {.i = -1 } },		// k
	{ MODKEY,                       44,        focusstackvis,     {.i = +1 } },		// j
	{ MODKEY,                       45,        focusstackvis,     {.i = -1 } },		// k
	{ MODKEY|ControlMask,                       44,        focusstackhid,     {.i = +1 } },		// j
	{ MODKEY|ControlMask,                       45,        focusstackhid,     {.i = -1 } },		// k
	{ MODKEY,                       31,        incnmaster,     {.i = +1 } },		// i
	{ MODKEY,                       40,        incnmaster,     {.i = -1 } },		// d
	{ MODKEY,                       43,        setmfact,       {.f = -0.05} },		// h
	{ MODKEY,                       46,        setmfact,       {.f = +0.05} },		// l
	{ MODKEY,                       36,        zoom,           {0} },			// Return
	{ MODKEY,                       23,        altTabStart,    {0} },			// Tab	
	{ MODKEY,			24,	   view,	   {0} },			// q
	{ MODKEY|ShiftMask,             54,        killclient,     {0} },			// c
	{ MODKEY,                       28,        setlayout,      {.v = &layouts[0]} },	// t
	{ MODKEY,                       41,        setlayout,      {.v = &layouts[1]} },	// f
	{ MODKEY,                       58,        setlayout,      {.v = &layouts[2]} },	// m
	{ MODKEY,                       27,        setlayout,      {.v = &layouts[3]} },	// r
	{ MODKEY,                       65,        setlayout,      {0} },			// Space
	{ MODKEY|ShiftMask,             65,        togglefloating, {0} },			// Space
	{ MODKEY,                       19,        view,           {.ui = ~0 } },		// 0
	{ MODKEY|ShiftMask,             19,        tag,            {.ui = ~0 } },		// 0
	{ MODKEY,                       59,        focusmon,       {.i = -1 } },		// Comma
	{ MODKEY,                       60,        focusmon,       {.i = +1 } },		// Period
	{ MODKEY|ShiftMask,             59,        tagmon,         {.i = -1 } },		// Comma
	{ MODKEY|ShiftMask,             60,        tagmon,         {.i = +1 } },		// Period
	{ MODKEY|ControlMask,		65,        focusmaster,	   { 0 } },			// Space
	{ MODKEY,                       39,      show,           {0} },				// s
	{ MODKEY|ShiftMask,             39,      showall,        {0} },				// s
	{ MODKEY|ShiftMask,             43,      hide,           {0} },				// h
	{ MODKEY|ControlMask,	24,  moveplace_quadrants,{.ui = WIN_NW } },			// q
	{ MODKEY|ControlMask,	25,  moveplace_quadrants,{.ui = WIN_NE } },			// w
	{ MODKEY|ControlMask,	26,  moveplace_quadrants,{.ui = WIN_SW } },			// e
	{ MODKEY|ControlMask,	27,  moveplace_quadrants,{.ui = WIN_SE } },			// r
	{ MODKEY|ControlMask,	38, 	   moveplace_horiz,{.ui = WIN_W} },			// a
	{ MODKEY|ControlMask,	39,	   moveplace_horiz,{.ui = WIN_E} },			// s
	{ MODKEY|ControlMask,	40,	    moveplace_vert,{.ui = WIN_N} },			// d
	{ MODKEY|ControlMask,	41,	    moveplace_vert,{.ui = WIN_S} },			// f
	{ MODKEY|ControlMask,	52,	  moveplace_sixths,{.ui = WIN_UL } },			// y
	{ MODKEY|ControlMask,	53,	  moveplace_sixths,{.ui = WIN_UC } },			// x
	{ MODKEY|ControlMask,	54,	  moveplace_sixths,{.ui = WIN_UR } },			// c
	{ MODKEY|ControlMask,	55,	  moveplace_sixths,{.ui = WIN_LL } },			// v
	{ MODKEY|ControlMask,	56,	  moveplace_sixths,{.ui = WIN_LC } },			// b
	{ MODKEY|ControlMask,	57,	  moveplace_sixths,{.ui = WIN_LR } },			// n
	{ MODKEY|ControlMask,	42,	  moveplace_thirds,{.ui = WIN_L} },			// g
	{ MODKEY|ControlMask,	43,	  moveplace_thirds,{.ui = WIN_C} },			// h
	{ MODKEY|ControlMask,	44,	  moveplace_thirds,{.ui = WIN_R} },			// j
	{ MODKEY|ControlMask,           114,       viewnext,       {0} },			// Right
	{ MODKEY|ControlMask,           113,       viewprev,       {0} },			// Left
	{ MODKEY|ShiftMask,             114,       tagtonext,      {0} },			// Right
	{ MODKEY|ShiftMask,             113,       tagtoprev,      {0} },			// Left
	TAGKEYS(                        10,                         0)				// 1 (or first key from top row)
	TAGKEYS(                        11,                         1)				// 2
	TAGKEYS(                        12,                         2)				// 3	
	TAGKEYS(                        13,                         3)				// 4
	TAGKEYS(                        14,                         4)				// 5
	TAGKEYS(                        15,                         5)				// 5
	TAGKEYS(                        16,                         6)				// 6
	TAGKEYS(                        17,                         7)				// 7
	TAGKEYS(                        18,                         8)				// 8
	{ MODKEY|ShiftMask,             24,        quit,           {0} },			// q
	{ MODKEY|ShiftMask,		27,	   quit,	   {1} },			// r
	{ MODKEY,                       30,        focusurgent,    {0} },			// u
	{ MODKEY,                       61,       scratchpad_show, {0} },			// Minus
	{ MODKEY|ShiftMask,             61,       scratchpad_hide, {0} },			// Minus
	{ MODKEY,                       20,      scratchpad_remove,{0} },			// Equal
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static Button buttons[] = {
	/* click                event mask      button          function        argument */
	{ ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
	{ ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
	{ ClkWinTitle,          0,              Button2,        zoom,           {0} },
	{ ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
	{ ClkClientWin,        MODKEY|ShiftMask,Button1,        togglefloating, {0} },
	{ ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
	{ ClkTagBar,            0,              Button1,        view,           {0} },
	{ ClkTagBar,            0,              Button3,        toggleview,     {0} },
	{ ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
	{ ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
	{ ClkLtSymbol,		0,		Button1,	setlayout,      {0} },
	{ ClkWinTitle,		0,		Button1,	togglewin,	{0} },
};

