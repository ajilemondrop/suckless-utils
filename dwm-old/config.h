/*        _ _ _                                _                 
*        (_|_) |                              | |                
*       _ _ _| | ___ _ __ ___   ___  _ __   __| |_ __ ___  _ __  
*   / _` | | | |/ _ \ '_ ` _ \ / _ \| '_ \ / _` | '__/ _ \| '_ \ 
*  | (_| | | | |  __/ | | | | | (_) | | | | (_| | | | (_) | |_) |
*   \__,_| |_|_|\___|_| |_| |_|\___/|_| |_|\__,_|_|  \___/| .__/ 
*       _/ |                                              | |    
*      |__/                                               |_|    
*/

/* See LICENSE file for copyright and license details. */

#include <X11/XF86keysym.h>

/* appearance */
static const unsigned int borderpx  = 3;        /* border pixel of windows */
static const unsigned int gappx     = 8;        /* gap pixel between windows */
static const unsigned int snap      = 4;        /* snap pixel */
static const unsigned int systraypinning = 0;   /* 0: sloppy systray follows selected monitor, >0: pin systray to monitor X */
static const unsigned int systrayonleft = 0;   	/* 0: systray in the right corner, >0: systray on left of status text */
static const unsigned int systrayspacing = 2;   /* systray spacing */
static const int systraypinningfailfirst = 1;   /* 1: if pinning fails, display systray on the first monitor, False: display systray on the last monitor*/
static const int showsystray        = 1;        /* 0 means no systray */
static const int showbar            = 1;        /* 0 means no bar */
static const int topbar             = 0;        /* 0 means bottom bar */
static const int focusonwheel       = 0;	/* O means focus on click*/
static const int user_bh            = 24;       /* 0 means that dwm will calculate bar height, >= 1 means dwm will user_bh as bar height */
static const char *fonts[]          = {"mononoki nerd font:size=11:style=Bold"};
static const char dmenufont[]       = "mononoki nerd font:size=11:style=Bold";
static const char col_purple[]	    = "#bd93f9";
static const char col_bg[]	    = "#1f1e29";
static const char col_text[]	    = "#f8f8f2";
static const char *colors[][3]      = {
	/*               fg         bg               border   */
	[SchemeNorm] = { col_text,  col_bg,  col_bg},
	[SchemeSel]  = { col_text,  col_bg,  col_purple},
	};

static const char *const autostart[] = {
	"pulseaudio", "--start", NULL,						/*start audio server*/
	"picom", "--no-fading-openclose", "-b", NULL, 				/*useful for window effects, picom compositor*/
	"dwmblocks", NULL,							/*dwmblocks is a seperate application for the dwm status bar*/
	"xset", "b", "off", NULL,						/*disables beeping sound*/
	"xwallpaper", "--zoom", "/usr/share/wallpapers/wallpaper.png", NULL,	/*enables wallpaper and points it to the location*/
	"nm-applet", NULL,							/*NetworkManager applet for a graphical interface with NetworkManager*/
	"xfce4-clipman", NULL,							/*Clipboard Manager*/
	"dunst", NULL,								/*notification service*/
	"battery-script.sh", NULL,						/*notifies about battery percentage*/
	"redshift", NULL,							/*lowers screen temperature at night*/
	NULL									/* terminate */
};

/* tagging */
//static const char *tags[] = { "", "", "", "", "", "", "", "", ""};
//static const char *tags[] = { "", "", "", "", "", "", "", "", "" };
static const char *tags[] = { "1", "2", "3", "4", "5", "6", "7", "8", "9"};

static const unsigned int ulinepad	= 5;	/* horizontal padding between the underline and tag */
static const unsigned int ulinestroke	= 5;	/* thickness / height of the underline */
static const unsigned int ulinevoffset	= 0;	/* how far above the bottom of the bar the line should appear */
static const int ulineall 		= 0;	/* 1 to show underline on all tags, 0 for just the active ones */

static const char *tagsel[][2] = { 		/* colours of tags, {foreground, background} */
	{ col_text, col_bg },
	{ col_text, col_bg },
	{ col_text, col_bg },
	{ col_text, col_bg },
	{ col_text, col_bg },
	{ col_text, col_bg },
	{ col_text, col_bg },
	{ col_text, col_bg },
	{ col_text, col_bg }

};

static const Rule rules[] = {
	/* xprop(1):
	 *	WM_CLASS(STRING) = instance, class
	 *	WM_NAME(STRING) = title
	 */
	/* class      instance    title       tags mask     isfloating   monitor */
	{ "Nm-connection-editor", NULL, NULL, 0,	    1,		 -1 },
	{ "Blueman-manager", 	  NULL,	NULL, 0,	    1,		 -1 },
	{ "Galculator"		, NULL, NULL, 0,	    1,		 -1 },
	{ "Pavucontrol",	  NULL, NULL, 0,	    1,		 -1 },
	{ "st-256color", NULL, "pulsemixer",  0,	    1,		 -1 },
};

/* layout(s) */
static const float mfact     = 0.50; /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;    /* number of clients in master area */
static const int resizehints = 1;    /* 1 means respect size hints in tiled resizals */
static const int decorhints  = 0;    /* 1 means respect decoration hints */
static const int attachbelow = 1;    /* 1 means attach after the currently active window */
static const int lockfullscreen = 1; /* 1 will force focus on the fullscreen window */

static void focusurgent (const Arg *arg);

static const Layout layouts[] = {
	/* symbol     arrange function */
	{ "[]=",      tile },    /* first entry is default */
	{ "><>",      NULL },    /* no layout function means floating behavior */
	{ "[M]",      monocle },
	{ "[D]",      deck },
};

/* key definitions */
#define MODKEY Mod1Mask
#define TAGKEYS(KEY,TAG) \
	{ MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} }, \
        { Mod4Mask,                     KEY,      focusnthmon,    {.i  = TAG } }, \
        { Mod4Mask|ShiftMask,           KEY,      tagnthmon,      {.i  = TAG } },


/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* commands */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
static const char *dmenucmd[] = { "dmenu_run", "-m", dmenumon, "-fn", dmenufont, "-nb", col_bg, "-nf", col_text, "-sb", col_purple, "-sf", col_bg, NULL };
static const char *powermenu [] = { "logout-script.sh", NULL };
static const char *termcmd[]  = { "st", NULL };
static const char *tabbedst[] = { "tabbed", "-c", "-r", "2", "st", "-w", "''", NULL };
static const char *brave[] = {"brave", NULL};
static const char *braveincognito[] = {"brave", "--incognito", NULL};
static const char *librewolf[] = {"librewolf", NULL};
static const char *filemanager[] = {"nemo", NULL };
static const char *signal_desktop[] = {"signal-desktop", NULL};
static const char *htop[] = {"st", "-e", "htop", NULL};
static const char *pulsemixer[] = {"st","-e", "pulsemixer", NULL};
static const char *volup[] = {"volume.sh", "up", NULL};
static const char *voldown[] = {"volume.sh", "down", NULL};
static const char *volmute[] = {"volume.sh", "mute", NULL};
static const char *ranger[] = {"st", "-e", "ranger", NULL};
static const char *keepass[] = {"keepassxc", "/home/ajilemondrop/.config/keepassxc/databases/passwords.kdbx", NULL};
static const char *brightnessup[] = {"brightness.sh", "up", NULL};
static const char *brightnessdown[] = {"brightness.sh", "down", NULL};
static const char *subs[] = {"ytfzf", "-D", "--scrape=youtube-subscriptions", "--sort-by=upload_date", NULL};
static const char *ytfzf[] = {"ytfzf", "-D", NULL};
static const char *stdash[] = {"st", "-e", "dash", NULL};
static const char *calcurse[] = {"st", "-e", "calcurse", NULL};
static const char *writer[] = {"libreoffice", "--writer", NULL};
static const char *impress[] = {"libreoffice", "--impress", NULL};
static const char *calc[] = {"libreoffice", "--calc", NULL};
static const char *audacity[] = {"audacity", NULL};
static const char *touchoff[] = {"xinput", "set-prop", "10", "Device Enabled", "1", NULL};
static const char *touchon[] = {"xinput", "set-prop", "10", "Device Enabled", "0", NULL};
static const char *links[] = {"tabbed", "-c", "-r", "2", "st", "-w", "''", "-e", "links", NULL};
static const char *thunderbird[] = {"thunderbird", NULL};
static const char *galculator[] = {"galculator", NULL};
static const char *gimp[] = {"gimp", NULL};
static const char *musescore[] = {"musescore", NULL};
static const char *screenshot[] = {"scrot", "/home/ajilemondrop/Pictures/screenshots/%Y-%m-%d-%H-%M-%S-screenshot.png", NULL};
static const char *gthumb [] = {"gthumb", "/home/ajilemondrop/Pictures", NULL};
static const char *pavucontrol [] = {"pavucontrol", NULL};
static const char *freetube[] = {"freetube", NULL};
static const char *tagger[] = {"easytag", NULL};


static Key keys[] = {
	/* modifier                     key        function        argument */
	{ MODKEY,                       XK_p,      spawn,          {.v = dmenucmd } },
	{ Mod1Mask|ControlMask,		XK_Delete, spawn,	   {.v = powermenu} },
	{ Mod4Mask,	                XK_t,      spawn,          {.v = termcmd } },
	{ Mod4Mask|ShiftMask,		XK_t,	   spawn,	   {.v = tabbedst } },
	{ MODKEY,                       XK_b,      togglebar,      {0} },
	{ Mod4Mask,			XK_b,	   spawn,	   {.v = brave} },
	{ Mod4Mask|ShiftMask,		XK_b,	   spawn,	   {.v = braveincognito} },
	{ Mod4Mask,			XK_e,	   spawn,	   {.v = filemanager} },
	{ Mod4Mask,			XK_s,	   spawn,	   {.v = signal_desktop} },
	{ Mod4Mask,			XK_h,	   spawn,	   {.v = htop} },
	{ Mod4Mask,			XK_r,	   spawn,	   {.v = ranger} },
	{ 0,		XF86XK_AudioLowerVolume,   spawn,	   {.v = voldown} },
	{ 0,		XF86XK_AudioRaiseVolume,   spawn,	   {.v = volup} },
	{ 0,		XF86XK_AudioMute,	   spawn,	   {.v = volmute } },
	{ Mod4Mask|ShiftMask,		XK_p,	   spawn,	   {.v = pulsemixer} },
	{ Mod4Mask,			XK_k,	   spawn,	   {.v = keepass} },
	{ 0,		XF86XK_MonBrightnessDown,  spawn,	   {.v = brightnessdown} },
	{ 0,		XF86XK_MonBrightnessUp,    spawn,	   {.v = brightnessup} },
	{ Mod4Mask|ShiftMask,		XK_y,	   spawn,	   {.v = subs} },
	{ Mod4Mask,			XK_y,	   spawn,	   {.v = ytfzf} },
	{ Mod4Mask,			XK_d,	   spawn,	   {.v = stdash} },
	{ Mod4Mask|ShiftMask,		XK_c,	   spawn,	   {.v = calcurse} },
	{ Mod4Mask,			XK_w,	   spawn,	   {.v = writer} },
	{ Mod4Mask,			XK_i,	   spawn,	   {.v = impress} },
	{ Mod4Mask,			XK_c,	   spawn,	   {.v = calc} },
	{ Mod4Mask,			XK_a,	   spawn,	   {.v = audacity} },
	{ Mod4Mask,			XK_space,  spawn,	   {.v = touchoff} },
	{ Mod4Mask|ShiftMask,		XK_space,  spawn,	   {.v = touchon} },
	{ Mod4Mask|ShiftMask,		XK_l,	   spawn,	   {.v = links} },
	{ Mod4Mask,			XK_l,	   spawn,	   {.v = librewolf} },
	{ Mod4Mask|ControlMask,		XK_t,	   spawn,	   {.v = thunderbird } },
	{ Mod4Mask|ShiftMask,		XK_g,	   spawn,	   {.v = galculator} },
	{ Mod4Mask,			XK_g,	   spawn,	   {.v = gimp} },
	{ Mod4Mask,			XK_m,	   spawn,	   {.v = musescore} },
	{ AnyKey,			XK_Print,  spawn,	   {.v = screenshot} },
	{ Mod4Mask|ControlMask,		XK_g,	   spawn,	   {.v = gthumb} },
	{ Mod4Mask,			XK_p,	   spawn,	   {.v = pavucontrol} },
	{ Mod4Mask,			XK_f,	   spawn,	   {.v = freetube} },
	{ Mod4Mask|ShiftMask,		XK_e,	   spawn,	   {.v = tagger} },
	{ MODKEY|ShiftMask,             XK_j,      rotatestack,    {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_k,      rotatestack,    {.i = -1 } },
	{ MODKEY,                       XK_j,      focusstack,     {.i = +1 } },
	{ MODKEY,                       XK_k,      focusstack,     {.i = -1 } },
	{ MODKEY,                       XK_i,      incnmaster,     {.i = +1 } },
	{ MODKEY,                       XK_d,      incnmaster,     {.i = -1 } },
	{ MODKEY,                       XK_h,      setmfact,       {.f = -0.05} },
	{ MODKEY,                       XK_l,      setmfact,       {.f = +0.05} },
	{ MODKEY,                       XK_Return, zoom,           {0} },
	{ MODKEY,                       XK_Tab,    view,           {0} },
	{ MODKEY|ShiftMask,             XK_c,      killclient,     {0} },
	{ MODKEY,                       XK_t,      setlayout,      {.v = &layouts[0]} },
	{ MODKEY,                       XK_f,      setlayout,      {.v = &layouts[1]} },
	{ MODKEY,                       XK_m,      setlayout,      {.v = &layouts[2]} },
	{ MODKEY,                       XK_r,      setlayout,      {.v = &layouts[3]} },
	{ MODKEY,                       XK_space,  setlayout,      {0} },
	{ MODKEY|ShiftMask,             XK_space,  togglefloating, {0} },
	{ MODKEY,                       XK_F10,    view,           {.ui = ~0 } },
	{ MODKEY|ShiftMask,             XK_F10,    tag,            {.ui = ~0 } },
	{ MODKEY,                       XK_comma,  focusmon,       {.i = -1 } },
	{ MODKEY,                       XK_period, focusmon,       {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_comma,  tagmon,         {.i = -1 } },
	{ MODKEY|ShiftMask,             XK_period, tagmon,         {.i = +1 } },
	{ MODKEY|ControlMask,		XK_space,  focusmaster,	   { 0 } },
	{ MODKEY|ControlMask,	XK_q,  moveplace_quadrants,{.ui = WIN_NW } },
	{ MODKEY|ControlMask,	XK_w,  moveplace_quadrants,{.ui = WIN_NE } },
	{ MODKEY|ControlMask,	XK_e,  moveplace_quadrants,{.ui = WIN_SW } },
	{ MODKEY|ControlMask,	XK_r,  moveplace_quadrants,{.ui = WIN_SE } },
	{ MODKEY|ControlMask,	XK_a, 	   moveplace_horiz,{.ui = WIN_W} },
	{ MODKEY|ControlMask,	XK_s,	   moveplace_horiz,{.ui = WIN_E} },
	{ MODKEY|ControlMask,	XK_d,	    moveplace_vert,{.ui = WIN_N} },
	{ MODKEY|ControlMask,	XK_f,	    moveplace_vert,{.ui = WIN_S} },
	{ MODKEY|ControlMask,	XK_y,	  moveplace_sixths,{.ui = WIN_UL } },
	{ MODKEY|ControlMask,	XK_x,	  moveplace_sixths,{.ui = WIN_UC } },
	{ MODKEY|ControlMask,	XK_c,	  moveplace_sixths,{.ui = WIN_UR } },
	{ MODKEY|ControlMask,	XK_v,	  moveplace_sixths,{.ui = WIN_LL } },
	{ MODKEY|ControlMask,	XK_b,	  moveplace_sixths,{.ui = WIN_LC } },
	{ MODKEY|ControlMask,	XK_n,	  moveplace_sixths,{.ui = WIN_LR } },
	{ MODKEY|ControlMask,	XK_g,	  moveplace_thirds,{.ui = WIN_L} },
	{ MODKEY|ControlMask,	XK_h,	  moveplace_thirds,{.ui = WIN_C} },
	{ MODKEY|ControlMask,	XK_j,	  moveplace_thirds,{.ui = WIN_R} },
	{ MODKEY|ControlMask,           XK_Right,  viewnext,       {0} },
	{ MODKEY|ControlMask,           XK_Left,   viewprev,       {0} },
	{ MODKEY|ShiftMask,             XK_Right,  tagtonext,      {0} },
	{ MODKEY|ShiftMask,             XK_Left,   tagtoprev,      {0} },
	TAGKEYS(                        XK_F1,                      0)
	TAGKEYS(                        XK_F2,                      1)
	TAGKEYS(                        XK_F3,                      2)
	TAGKEYS(                        XK_F4,                      3)
	TAGKEYS(                        XK_F5,                      4)
	TAGKEYS(                        XK_F6,                      5)
	TAGKEYS(                        XK_F7,                      6)
	TAGKEYS(                        XK_F8,                      7)
	TAGKEYS(                        XK_F9,                      8)
	{ MODKEY|ShiftMask,             XK_q,      quit,           {0} },
	{ MODKEY,                       XK_u,      focusurgent,    {0} },
	{ MODKEY,                       XK_minus, scratchpad_show, {0} },
	{ MODKEY|ShiftMask,             XK_minus, scratchpad_hide, {0} },
	{ MODKEY,                       XK_equal,scratchpad_remove,{0} },
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static Button buttons[] = {
	/* click                event mask      button          function        argument */
	{ ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
	{ ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
	{ ClkWinTitle,          0,              Button2,        zoom,           {0} },
	{ ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
	{ ClkClientWin,        MODKEY|ShiftMask,Button1,        togglefloating, {0} },
	{ ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
	{ ClkTagBar,            0,              Button1,        view,           {0} },
	{ ClkTagBar,            0,              Button3,        toggleview,     {0} },
	{ ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
	{ ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
	{ ClkWinTitle,		0,		Button1,	scratchpad_hide,{0} },
	{ ClkWinTitle,		0,		Button3,	scratchpad_show,{0} },
	{ ClkWinTitle,		ShiftMask,	Button1,      scratchpad_remove,{0} },
	{ ClkLtSymbol,		0,		Button1,	setlayout,      {0} },
};

