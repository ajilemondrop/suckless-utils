/*       _ _ _                                _                 
        (_|_) |                              | |                
       _ _ _| | ___ _ __ ___   ___  _ __   __| |_ __ ___  _ __  
   / _` | | | |/ _ \ '_ ` _ \ / _ \| '_ \ / _` | '__/ _ \| '_ \ 
  | (_| | | | |  __/ | | | | | (_) | | | | (_| | | | (_) | |_) |
   \__,_| |_|_|\___|_| |_| |_|\___/|_| |_|\__,_|_|  \___/| .__/ 
       _/ |                                              | |    
      |__/                                               |_|    
*/

//Modify this file to change what commands output to your statusbar, and recompile using the make command.
static const Block blocks[] = {
	/*Icon*/	/*Command*/		/*Update Interval*/	/*Update Signal*/

	{"|   ", "date '+%A, %d. %B, %H:%M'",					5,		0},
	{" ", "brightnessctl | grep Current | awk -F ' ' '{print $3}'", 1, 0},
//	{"  ", "acpi -b | awk -F', ' '{print $2}'", 	60, 0},
//	{"", "battery-icon-script-new.sh", 		1,			0},
	{"", "battery-icon-script.sh", 		1,			0},
	{" ", "pulsemixer --get-volume | awk -F ' ' '{ print $1 }'", 1, 0}
};

//sets delimeter between status commands. NULL character ('\0') means no delimeter.
static char delim[] = " | ";
static unsigned int delimLen = 5;
