/*       _ _ _                                _                 
        (_|_) |                              | |                
       _ _ _| | ___ _ __ ___   ___  _ __   __| |_ __ ___  _ __  
   / _` | | | |/ _ \ '_ ` _ \ / _ \| '_ \ / _` | '__/ _ \| '_ \ 
  | (_| | | | |  __/ | | | | | (_) | | | | (_| | | | (_) | |_) |
   \__,_| |_|_|\___|_| |_| |_|\___/|_| |_|\__,_|_|  \___/| .__/ 
       _/ |                                              | |    
      |__/                                               |_|    

See LICENSE file for copyright and license details. */

/* appearance */
static const char font[]        = "mononoki nerd font:size=11:style=bold";
static const char* normbgcolor  = "#1f1e29";
static const char* normfgcolor  = "#6272a4";
//static const char* selbgcolor   = "#bd93f9";
//static const char* selfgcolor   = "#1f1e29";
static const char* selbgcolor   = "#1f1e29";
static const char* selfgcolor   = "#bd93f9";
static const char* urgbgcolor   = "#50fa7b";
static const char* urgfgcolor   = "#f8f8f2";
static const char before[]      = "<";
static const char after[]       = ">";
static const char titletrim[]   = "...";
static const int  tabwidth      = 200;
static const Bool foreground    = True;
static       Bool urgentswitch  = False;
static const int barHeight	= 24;

/*
 * Where to place a new tab when it is opened. When npisrelative is True,
 * then the current position is changed + newposition. If npisrelative
 * is False, then newposition is an absolute position.
 */
static int  newposition   = 0;
static Bool npisrelative  = False;

#define SETPROP(p) { \
        .v = (char *[]){ "/bin/sh", "-c", \
                "prop=\"`xwininfo -children -id $1 | grep '^     0x' |" \
                "sed -e's@^ *\\(0x[0-9a-f]*\\) \"\\([^\"]*\\)\".*@\\1 \\2@' |" \
                "xargs -0 printf %b | dmenu -l 10 -w $1`\" &&" \
                "xprop -id $1 -f $0 8s -set $0 \"$prop\"", \
                p, winid, NULL \
        } \
}

#define MODKEY ControlMask
static Key keys[] = {
	/* modifier                  key    function        argument */
	{ MODKEY|ShiftMask,          28,    focusonce,      { 0 } },      // t
	{ MODKEY|ShiftMask,          28,    spawn,          { 0 } },      // t
	{ MODKEY,                    44,    spawn,          SETPROP("_TABBED_SELECT_TAB") }, // t

	{ MODKEY|ShiftMask,          46,   rotate,         { .i = +1 } }, // l
	{ MODKEY|ShiftMask,          43,   rotate,         { .i = -1 } }, // h
	{ MODKEY|ShiftMask,          44,   movetab,        { .i = -1 } }, // j
	{ MODKEY|ShiftMask,          45,   movetab,        { .i = +1 } }, // k
	{ MODKEY,                    23,   rotate,         { .i = 0 } },  // Tab

	{ MODKEY,                    10,   move,           { .i = 0 } },  // 1
	{ MODKEY,                    11,   move,           { .i = 1 } },  // 2
	{ MODKEY,                    12,   move,           { .i = 2 } },  // 3
	{ MODKEY,                    13,   move,           { .i = 3 } },  // 4
	{ MODKEY,                    14,   move,           { .i = 4 } },  // 5
	{ MODKEY,                    15,   move,           { .i = 5 } },  // 6
	{ MODKEY,                    16,   move,           { .i = 6 } },  // 7
	{ MODKEY,                    17,   move,           { .i = 7 } },  // 8
	{ MODKEY,                    18,   move,           { .i = 8 } },  // 9
	{ MODKEY,                    19,   move,           { .i = 9 } },  // 0

	{ MODKEY|ShiftMask,          25,   killclient,     { 0 } },       // w

	{ 0,                         95,   fullscreen,     { 0 } },       // F11

	{ MODKEY|ShiftMask,	     30,   focusurgent,	   { 0 } },	  // u
	{ MODKEY|ShiftMask,	     65, toggle, {.v = (void*) &urgentswitch } }, // Space
};
